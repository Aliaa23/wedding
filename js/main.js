var mybutton = document.getElementById("mybtn");
window.onscroll = function scroll() {

    if (window.pageYOffset > 100) {
        mybutton.style.display = "block";

    } else {
        mybutton.style.display = "none";
    }

}
mybutton.onclick = function click() {
    window.scrollTo({
        top: 0,
        behavior: "smooth"
    });
}

var menubtn = document.getElementById('menu-button');
var submenu = document.getElementById('menu');

menubtn.onmouseover = function() {
    submenu.style.display = "block";
}
menubtn.onmouseleave = function() {
    submenu.style.display = "none";
}